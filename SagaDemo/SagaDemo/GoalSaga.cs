﻿using System;

namespace SagaDemo
{
    public class GoalSaga : IGoalSaga
    {
        Guid id;
        string name;
        DateTime startTime;
        DateTime endTime;

        ProcessStatus status;

        public GoalSaga (Guid id, string name, DateTime startTime, DateTime endTime)
        {
            this.id = id;
            this.name = name;
            this.startTime = startTime;
            this.endTime = endTime;

            status = ProcessStatus.NotStarted;

            // Enter goal into DB.
            // Set start timeout.
        }

        public void EnrolUserInTeam (Guid userId, Guid teamId)
        {
            // Add user/team/goal combo to DB.
        }

        public void DeenrolUserInTeam (Guid userId, Guid teamId)
        {
            // Remove user/team/goal combo from DB.
        }

        public event GoalStatusChangedEventHandler StatusChanged;

        void OnStartTimeout()
        {
            status = ProcessStatus.InProgress;

            // Set end timeout.
        }

        void OnEndTimeout()
        {
            // Mark ourselves as failed if we haven't succeeded by this point.
        }

        void OnTaskStatusChanged ()
        {
            // Update DB with our completion status based on the status of all our users' tasks.
            StatusChanged(this, new GoalStatusChangedEventArgs { Id = id, Status = status, completionTime = DateTime.Now });
        }

        void HandleDeleteGoalCommand (Guid id)
        {
            // Remove goal from DB.
        }
    }
}