﻿using System;

namespace SagaDemo
{
    public class MissionSaga : IMissionSaga
    {
        Guid id;
        string name;
        DateTime startTime;
        DateTime endTime;

        ProcessStatus status;

        public MissionSaga (Guid id, string name, DateTime startTime, DateTime endTime)
        {
            this.id = id;
            this.name = name;
            this.startTime = startTime;
            this.endTime = endTime;

            status = ProcessStatus.NotStarted;

            // Enter mission into DB.
            // Set start timeout.
        }

        public void CreateGoal (Guid id, string name, DateTime startTime, DateTime endTime)
        {
            // Send CreateGoal command to start relevant saga and write to DB.
        }

        public void DeleteGoal (Guid id)
        {
            // Send DeleteGoal command to end relevant saga.
        }

        public void EnrolUserInTeam (Guid userId, Guid teamId)
        {
            // Add user/team/mission combo to DB.
            // Tell all our goals to enrol the user in the team.
        }

        public void DeenrolUserInTeam (Guid userId, Guid teamId)
        {
            // Remove user/team/mission combo from DB.
            // Tell all our goals to deenrol the user in the team.
        }

        public event MissionStatusChangedEventHandler StatusChanged;

        void OnStartTimeout()
        {
            status = ProcessStatus.InProgress;

            // Set end timeout.
        }

        void OnEndTimeout ()
        {
            // Mark ourselves as failed if we haven't succeeded by this point.
        }

        void OnGoalStatusChanged (IGoalSaga sender, GoalStatusChangedEventArgs args)
        {
            // Update DB with our completion status based on the status of all our goals.
            StatusChanged(this, new MissionStatusChangedEventArgs { Id = id, Status = status, completionTime = DateTime.Now });
        }

        void HandleDeleteMissionCommand (Guid id)
        {
            // Send DeleteGoal commands for all our goals.
            // Remove mission from DB.
        }
    }
}