﻿using System;

namespace SagaDemo
{
    public class QuestSaga : IQuestSaga
    {
        Guid id;
        string name;
        DateTime startTime;
        DateTime endTime;

        ProcessStatus status;

        public QuestSaga (Guid id, string name, DateTime startTime, DateTime endTime)
        {
            this.id = id;
            this.name = name;
            this.startTime = startTime;
            this.endTime = endTime;

            status = ProcessStatus.NotStarted;

            // Enter quest into DB.
            // Set start timeout.
        }

        public void CreateMission (Guid id, string name, DateTime startTime, DateTime endTime)
        {
            // Send CreateMission command to start relevant saga and write to DB.
        }

        public void DeleteMission (Guid id)
        {
            // Send DeleteMission command to end relevant saga.
        }

        public void EnrolUserInTeam (Guid userId, Guid teamId)
        {
            // Add user/team/quest combo to DB.
            // Tell all our missions to enrol the user in the team.
        }

        public void DeenrolUserInTeam (Guid userId, Guid teamId)
        {
            // Remove user/team/quest combo from DB.
            // Tell all our missions to deenrol the user in the team.
        }

        public event QuestStatusChangedEventHandler StatusChanged;

        void OnStartTimeout ()
        {
            status = ProcessStatus.InProgress;

            // Set end timeout.
        }

        void OnEndTimeout ()
        {
            // Mark ourselves as failed if we haven't succeeded by this point.
        }

        void OnMissionStatusChanged (IMissionSaga sender, MissionStatusChangedEventArgs args)
        {
            // Update DB with our completion status based on the status of all our missions.
            StatusChanged(this, new QuestStatusChangedEventArgs { Id = id, Status = status, completionTime = DateTime.Now });
        }

        void HandleDeleteQuestCommand (Guid id)
        {
            // Send DeleteMission commands for all our missions.
            // Remove quest from DB.
        }
    }
}