﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SagaDemo
{
    public class GoalStatusChangedEventArgs : EventArgs
    {
        public Guid Id;
        public ProcessStatus Status;
        public DateTime? completionTime;
    }
}