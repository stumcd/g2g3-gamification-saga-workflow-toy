﻿using System;

namespace SagaDemo
{
    public interface IMissionSaga
    {
        void CreateGoal (Guid id, string name, DateTime startTime, DateTime endTime);
        void DeleteGoal (Guid id);

        void EnrolUserInTeam (Guid userId, Guid teamId);
        void DeenrolUserInTeam (Guid userId, Guid teamId);

        event MissionStatusChangedEventHandler StatusChanged;
    }
}