﻿using System;

namespace SagaDemo
{
    public interface IGoalSaga
    {
        void EnrolUserInTeam (Guid userId, Guid teamId);
        void DeenrolUserInTeam (Guid userId, Guid teamId);

        event GoalStatusChangedEventHandler StatusChanged;
    }
}