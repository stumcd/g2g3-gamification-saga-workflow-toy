﻿using System;

namespace SagaDemo
{
    public interface IQuestSaga
    {
        void CreateMission (Guid id, string name, DateTime startTime, DateTime endTime);
        void DeleteMission (Guid id);

        void EnrolUserInTeam (Guid userId, Guid teamId);
        void DeenrolUserInTeam (Guid userId, Guid teamId);

        event QuestStatusChangedEventHandler StatusChanged;
    }
}